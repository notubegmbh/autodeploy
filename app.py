import json
import thread
from git import Repo
from flask import Flask, request
from datetime import datetime

app = Flask(__name__)

class Push(object):
	source = None
	repoowner = None
	reponame = None
	author = None
	commit = None
	message = None

	def __init__(self, data):
		pass

	@property
	def repo(self):
		return '/'.join([self.source, self.repoowner, self.reponame])

class BitbucketPush(Push):
	source = 'bitbucket'
	def __init__(self, data):
		self.commit = data['commits'][0]['raw_node']
		self.message = data['commits'][0]['message']
		self.author = data['commits'][0]['author']
		self.repoowner = data['repository']['owner']
		self.reponame = data['repository']['slug']

class GithubPush(Push):
	source = 'github'
	def __init__(self, data):
		self.source = 'github'
		self.commit = data['head_commit']['id']
		self.message = data['head_commit']['message']
		self.author = data['head_commit']['committer']['username']
		self.repoowner = data['repository']['owner']['name']
		self.reponame = data['repository']['name']

def readconfig(element=None):
	with open('config.json', 'r') as fd:
		config = json.loads(fd.read())
	if element:
		return config.get(element)
	return config
def log(text):
	print "[%s] %s" % (datetime.now().strftime("%Y-%m-%d %H:%M"), text)

def refresh(path):
	repo = Repo(path)
	repo.remotes.origin.pull()

@app.route('/', methods=['POST'])
def receive():
	log("Connection established.")
	if request.json:
	        data = request.json
	elif request.form.get('payload'):
	        data = json.loads(request.form.get('payload'))
	else:
	        log("Unable to find data. [ERROR]")
	        log("Closing connection.")
	        return "Where is it?", 500
	if data.get('canon_url') == 'https://bitbucket.org':
		log("Receiving data from bitbucket.")
		push = BitbucketPush(data)
	if data.get('compare'):
		log("Receiving data from github.")
		push = GithubPush(data)
	else:
		log("Received data from unknown source. [ERROR]")
		return "Who are you?", 500
	repos = readconfig('repos')
	for repo in repos:
		if '/'.join([repo['source'], repo['owner'], repo['name']]) == push.repo:
			log("Found instance of %s, updating to:" % push.repo)
			log("%s by %s (%s)" % (push.message, push.author, push.commit))
			thread.start_new_thread(refresh, (repo['path'],))
	return "Thanks.", 200
	log("Closing connection.")

# this checks whether the config exists at all
readconfig()
if __name__ == '__main__':
	app.run(port=readconfig('port'))
