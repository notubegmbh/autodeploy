# autodeploy

This autodeploy tool is multi capable. It can receive pushs from bitbucket and github (at the same time). It can also handle multiple instances of the same repository, to enable you to run different staging environments of your software.

## Setup
	```
	git clone git@bitbucket.org:notubegmbh/autodeploy.git
	pip install -r requirements.txt
	cp config.json.example config.json
	# edit the config to your needs
	python app.py
	```

The setup is very straightforward, as the configuration should be. You can use a reverse proxy with apache or nginx (or any other webserver you would like), to hide this tool behind a proper webserver and make use of gunicorn or similar. The described way is just the easiest way to do it.

If you have question you can reach me as @alexanderjulo at twitter, or via mail at alexander.jung-loddenkemper@notube.com.

